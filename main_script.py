#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from ipynb.fs.full.TBTlogs_fetch_New import fetch
from pathlib import Path
import csv
import signal
import bisect
from contextlib import contextmanager
class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)
 class Date:
    def __init__(self, d, m, y):
        self.d = d
        self.m = m
        self.y = y
        
def clean(filename,gen=1):
    data=pd.read_csv(filename)
    col=['MachineTS', 'ExchTstamp', 'SeqNum', 'SecurityId', 'Type', 'Side',
           'bid_size[0]', 'bid[0]', 'ask[0]', 'ask_size[0]', 'bid_size[1]',
           'bid[1]', 'ask[1]', 'ask_size[1]', 'bid_size[2]', 'bid[2]', 'ask[2]',
           'ask_size[2]', 'bid_size[3]', 'bid[3]', 'ask[3]', 'ask_size[3]',
           'bid_size[4]', 'bid[4]', 'ask[4]', 'ask_size[4]', 'price', 'qty',
           'old_price', 'old_qty', 'likely_bid_size', 'likely_bid', 'likely_ask',
           'likely_ask_size', 'btc', 'atc', 'btv', 'atv', 'dprLow', 'dprHigh',
           'ExchId1', 'ExchId2']
    lil=[]
    li=[]
    lili=['MachineTS','SeqNum','SecurityId','Side','bid_size[2]', 'bid[2]', 'ask[2]','ask_size[2]', 
          'bid_size[3]', 'bid[3]', 'ask[3]', 'ask_size[3]','bid_size[4]', 'bid[4]', 'ask[4]', 'ask_size[4]',
          'likely_bid_size', 'likely_bid', 'likely_ask','likely_ask_size','dprLow', 'dprHigh','ExchId1', 'ExchId2']
    if(gen):
        for i in lili:
            lil.append(data.columns[col.index(i)])
        data=data.drop(lil,axis=1)
        data.to_csv(filename, header=True, index=False)
    for x in col:
        if x not in lili:
            li.append(x)
    return data,li

def conv_list(data,i):
    x=pd.DataFrame(data,columns=[i])
    return list(x[i])

monthDays = [31, 28, 31, 30, 31, 30,
             31, 31, 30, 31, 30, 31]

def cLY(d):
    years = d.y
    if (d.m <= 2):
        years -= 1
    ans = int(years / 4)
    ans -= int(years / 100)
    ans += int(years / 400)
    return ans

def getDifference(dt1, dt2):
    n1 = dt1.y * 365 + dt1.d
    for i in range(0, dt1.m - 1):
        n1 += monthDays[i]
    n1 += cLY(dt1)
    n2 = dt2.y * 365 + dt2.d
    for i in range(0, dt2.m - 1):
        n2 += monthDays[i]
    n2 += cLY(dt2)
    return (n2 - n1)
 
def get_(date,month,hr,minn,sec):
    dt1 = Date(1, 1, 1970)
    dt2 = Date(date, month, 2021)
    xx=(((getDifference(dt1, dt2))*3600*24)-(3600*5+60*30))*1e9
    xx+=((hr*3600+minn*60+sec)*1e9)
    return xx
    
def get_tstmp(date,month,ets): 
    dt1 = Date(1, 1, 1970)
    dt2 = Date(date, month, 2021)
    xx=(getDifference(dt1, dt2))*3600.0*24.0
    sec=(ets/1e9-xx)+(3600*5+60*30)
    sol=""
    hr=int(sec//3600)
    if(hr//10):
        sol+=str(hr)+":"
    else:
        sol+="0"+str(hr)+":"
    mins=int((sec-3600*hr)//60)
    if(mins//10):
        sol+=str(mins)+":"
    else:
        sol+="0"+str(mins)+":"
    secs=((sec-3600*hr)-mins*60)
    if(secs//10):
        sol+=str(secs)
    else:
        sol+="0"+str(secs)
    return sol

def gr_in(ll,val):
    return min(bisect.bisect_left(ll, val, lo=0, hi=len(ll)),len(ll)-1)

def find_in_list_greater_(li,x):
    i=0
    while i<len(li):
        if abs(li[i][0])<x:
            return i
        else:
            i+=1
    return -1

def date_csv_format(date,month,year=2021):
    return str(date)+'/'+str(month)+'/'+str(year%100)

def tell_month(month):
    if month==1:
        return "JAN"
    elif month==2:
        return "FEB"
    elif month==3:
        return "MAR"
    elif month==4:
        return "APR"
    elif month==5:
        return "MAY"
    elif month==6:
        return "JUN"
    elif month==7:
        return "JULY"
    elif month==8:
        return "AUG"
    elif month==9:
        return "SEP"
    elif month==10:
        return "OCT"
    elif month==11:
        return "NOV"
    else:
        return "DEC"
  
def convert_date_to_string(date,month,is_deadline=0,year=2021):
    if(date<10):
        if(month<10 and is_deadline==0):
            datee=str(year)+"0"+str(month)+"0"+str(date)
        else:
            datee=str(year%100)+str(month)+"0"+str(date)
    else:
        if(month<10 and is_deadline==0):
            datee=str(year)+"0"+str(month)+str(date)
        else:
            datee=str(year%100)+str(month)+str(date)
    return datee

def futures_file_name(date,month,year=2021):
    if(month==4):
        filename="dinfra_tbt_64420_"
        filename+=convert_date_to_string(date,month)
        filename+=".csv"
        return filename
    elif month==5:
        filename="dinfra_tbt_61195_"
        filename+=convert_date_to_string(date,month)
        filename+=".csv"
        return filename
    else:
        filename="dinfra_tbt_48506_"
        filename+=convert_date_to_string(date,month)
        filename+=".csv"
        return filename
    
def opt_analysis(date,month,opt_pr,op_name,dic_opt_pr,datao,lili,ecp,price,etsf,time_delay):
    global done_idx
    atv=conv_list(datao,datao.columns[lili.index('atv')])
    btv=conv_list(datao,datao.columns[lili.index('btv')])
    etso=conv_list(datao,datao.columns[lili.index('ExchTstamp')])
    fut_chg=[]
    fut_pr=[]
    time_lag=[]
    vol_chg=[]
    time_dur=[]
    opt_name=[]
    for i in range(len(dic_opt_pr)):
        if(opt_pr<(price[ecp[dic_opt_pr[i]][1]]/1e2)):
            opt_name.append(str(opt_pr)+" PE")
            if(ecp[dic_opt_pr[i]][0]>0):
                look_for_atv=0
            else:
                look_for_atv=1
        else:
            opt_name.append(str(opt_pr)+" CE")
            if(ecp[dic_opt_pr[i]][0]>0):
                look_for_atv=1
            else:
                look_for_atv=0
        index=ecp[dic_opt_pr[i]][1]
        rwa=(get_tstmp(date,month,etsf[index]))
        upper_index=gr_in(etso,get_(date,month,int(rwa[:2]),int(rwa[3:5]),float(rwa[6:])))
        allowed_time=etsf[index]-1e9*time_delay
        rrwa=(get_tstmp(date,month,allowed_time))
        lower_index=gr_in(etso,get_(date,month,int(rrwa[:2]),int(rrwa[3:5]),float(rrwa[6:])))
        fut_chg.append(ecp[dic_opt_pr[i]][0])
        fut_pr.append(price[ecp[dic_opt_pr[i]][1]])
        time_lag.append(-1)
        vol_chg.append(-1)
        time_dur.append(-1)
        if look_for_atv:
            for j in range(upper_index-1,max(lower_index-2,0),-1):
                if (atv[j]-atv[j-1]>=500):
                    vol_chg[-1]=(atv[j]-atv[j-1])
                    time_dur[-1]=etso[j]-etso[j-1]
                    time_lag[-1]=(etsf[index]-etso[j])/1e9
                    done_idx[dic_opt_pr[i]]=1
                    break
        else:
            for j in range(upper_index-1,max(lower_index-2,0),-1):
                if (btv[j]-btv[j-1]>=500):
                    vol_chg[-1]=(btv[j]-btv[j-1])
                    time_dur[-1]=etso[j]-etso[j-1]
                    time_lag[-1]=(etsf[index]-etso[j])/1e9
                    done_idx[dic_opt_pr[i]]=1
#                     fticks=15
#                     ddit[convert_date_to_string(date,month)+str(opt_pr)]=[]
#                     for k in range(upper_index,min(upper_index+fticks,len(btv))):
#                         ddit[convert_date_to_string(date,month)+str(opt_pr)].append([btv[k],etso[k]])
#                         ddit[convert_date_to_string(date,month)+str(opt_pr)][k]-=ddit[convert_date_to_string(date,month)+str(opt_pr)][max]
                    break
    return fut_pr,fut_chg,time_lag,vol_chg,time_dur,opt_name

def deadline(date,month,year=2021):
#     thursday on 1st Jan 1970
    dt1=Date(date,month,year)
    dt2=Date(1,1,1970)
    x=getDifference(dt2,dt1)
    y=x%7
    if y==0:
        d_date=date
    else:
        d_date=date+(7-y)
    if(d_date+7>monthDays[month-1]):
        return ""
    if(d_date==13 and month==5):
        d_date-=1
    return convert_date_to_string(d_date,month,1,year)

def summarize_anlys(date,month,ecp,done_idx):
    pos=0
    neg=0
    r_pos=0
    r_neg=0
    for i in range(len(done_idx)):
        if ecp[i][0]>0:
            pos+=1
            if done_idx[i]==1:
                r_pos+=1
        else:
            neg+=1
            if done_idx[i]==1:
                r_neg+=1
    details = {
    'Up/Down Movement' : ['Up', 'Down'],
    'No of inst. analyzed' : [pos,neg],
    'No of inst. lead/lag beh. holds for' : [r_pos, r_neg],
    }
    df = pd.DataFrame(details, index = [1,2])
    print(df)

    
def is_atv_or_btv(x,y,z):
    if y<z:
        if x>0:
            return "BTV"
        else:
            return "ATV"
    else:
        if x>0:
            return "ATV"
        else:
            return "BTV"

def list_of_opt(price):
    l=[]
    ll=[]
    x=(price//1e4)*1e2
    if ((price//1e2)%1e2)==0:
        x-=100
    l=[]
    no=10
    while no:
        if x%500!=0:
            l.append(int(x))
            ll.append("PE")
            no-=1
        x-=100
    no=6
    x=(price//1e4)*1e2
    if ((price//1e2)%5e2)==0:
        x-=500
    else:
        x-=(((price//1e4)*1e2)%5e2)
    while no:
        l.append(int(x))
        ll.append("PE")
        x-=500
        no-=1
        
    x=(price//1e4)*1e2+100
    no=10
    while no:
        if x%500!=0:
            l.append(int(x))
            ll.append("CE")
            no-=1
        x+=100
    no=6
    x=(price//1e4)*1e2
    if ((price//1e2)%5e2)==0:
        x+=500
    else:
        x+=(500-(((price//1e4)*1e2)%5e2))
    while no:
        l.append(int(x))
        ll.append("CE")
        x+=500
        no-=1
    return l,ll       
        
def fut_analysis(date,month,deadline_month=0):
    global fut_dict
    if(deadline_month==0):
        deadline_month=month
    datee=convert_date_to_string(date,month)
    dataf=[]
    if datee+str(deadline_month) in fut_dict:
        file_name = Path(fut_dict[datee+str(deadline_month)])
        if file_name.exists()==False:
            try:
                fn=fetch(date=datee,month=tell_month(deadline_month))[0]
                dataf,lilil=clean(fn)
                fut_dict[datee+str(deadline_month)]=fn
            except:
                return -1
        else:
            print("FUTURES FILE ALREADY EXISTS")
            dataf,lilil=clean(file_name,0)
    else:
        try:
            fn=fetch(date=datee,month=tell_month(deadline_month))[0]
            dataf,lilil=clean(fn)
            print("downloaded")
            fut_dict[datee+str(deadline_month)]=fn
        except:
            return -1
    price=conv_list(dataf,dataf.columns[lilil.index('price')])
    etsf=conv_list(dataf,dataf.columns[lilil.index('ExchTstamp')])
    ch_pri=[]
    cummulative_pri=[]
    ecp=[]
    sign=10
    prev_sign=11
    sign_count=0
    for i in range(1,len(price)):
        ch_pri.append(price[i]-price[max(i-1,0)])
        if ch_pri[-1]>0:
            sign=1
        elif ch_pri[-1]<0:
            sign=-1
        else:
            sign=0
        if i!=1 and (sign==0 or prev_sign==sign):
            if sign_count<3:
                cummulative_pri.append([cummulative_pri[-1][0]+ch_pri[-1],cummulative_pri[-1][1]])
            else:
                cummulative_pri.append([ch_pri[-1],i-1])
                sign_count=0
            if abs(ch_pri[-1])<1200:
                sign_count+=1
            else:
                sign_count=0
            ecp.append(cummulative_pri[-1])
            if(np.mean(np.abs(ch_pri[max(0,i-10):i]))>3500) or abs(np.max(np.abs(ch_pri[max(i-14,0):-1])))>7000:
                ecp[-1]=[0,i]
        else:
            cummulative_pri.append([ch_pri[-1],max(i-1,0)])
            ecp.append(cummulative_pri[-1])
            if(np.mean(np.abs(ch_pri[max(0,i-10):i]))>3500) or (i>1 and abs(np.max(np.abs(ch_pri[max(i-14,0):-1])))>7000):
                ecp[-1]=[0,i]
        prev_sign=sign
    ri=[]
    c=0
    for i in range(1,len(cummulative_pri)):
        if cummulative_pri[i][1]==cummulative_pri[i-1][1]:
            ri.append(i-1-c)
            c+=1
    for i in range(len(ri)):
        del ecp[ri[i]]
    ecp.sort(reverse=True,key= lambda x:abs(x[0]))
    return price,etsf,ecp


def analysis_for_an_option(date,month,option_price,op_name,dic_opt_pr,time_delay,ecp,price,etsf):
    global opt_dict,done_idx
    datee=convert_date_to_string(date,month)
    datao=[]
    if datee+str(option_price)+op_name in opt_dict:
        file_name = Path(opt_dict[datee+str(option_price)+op_name])
        if file_name.exists()==False:
            try:
                with time_limit(90):
                    fn=fetch(date=datee,opt=op_name,x=option_price,deadline=deadline(date,month),month=tell_month(month))[0]
                    datao,lili=clean(fn)
                    opt_dict[datee+str(option_price)+op_name]=fn
            except:
                return [-1]
        else:
            print("OPTIONS FILE ALREADY EXISTS")
            datao,lili=clean(file_name,0)
    else:
        try:
            with time_limit(90):
                fn=fetch(date=datee,opt=op_name,x=option_price,deadline=deadline(date,month),month=tell_month(month))[0]
                datao,lili=clean(fn)
                opt_dict[datee+str(option_price)+op_name]=fn
        except:
            return [-1]
    fut_pr,fut_chg,time_lag,vol_chg,time_dur,opt_name=opt_analysis(date,month,option_price,op_name,dic_opt_pr,datao,lili,ecp,price,etsf,time_delay)
    return [time_lag,fut_pr,fut_chg,vol_chg,time_dur,opt_name]

def analysis_for_date(date,month,no_of_instances_,filename1,filename2,time_delay,deadline=0):
    global done_idx
    fields = ['Date', 'Option', 'Future Price', 'Fut Move','Vol. Move','Dur. of Vol Chg','Lead/Lag Time','ATV/BTV']
    price,etsf,ecp=fut_analysis(date,month,deadline)
    no_of_instances=find_in_list_greater_(ecp,5000)
    no_of_instances=min(no_of_instances,no_of_instances_)
    for i in range(no_of_instances):
        done_idx.append(0)
    l=[]
    pc=[]
    op_dict={}
    for i in range(len(ecp[:min(no_of_instances,len(ecp))])):
        a,b=list_of_opt(price[ecp[i][1]])
        for j in range(len(a)):
            if str(a[j])+b[j] in op_dict:
                op_dict[str(a[j])+b[j]].append(i)
            else:
                op_dict[str(a[j])+b[j]]=[i]
    for i in op_dict:
        output=analysis_for_an_option(date,month,int(i[:5]),i[5:],op_dict[i],time_delay,ecp[:min(no_of_instances,len(ecp))],price,etsf)
        if(len(output)==1):
            print("Cannot Fetch the Option Data due to an error for ",i)
            continue
        length=len(output[0])
        mydict=[]
        ini=[]
        for k in range(length):
            if output[0][k]!=-1:
                ini.append(k)
                mydict.append({'Date':date_csv_format(date,month),'Option':output[5][k],'Future Price':str(output[1][k]),'Fut Move':str(output[2][k]),'Vol. Move':str(output[3][k]),'Dur. of Vol Chg':str(output[4][k]),'Lead/Lag Time':str(output[0][k]),'ATV/BTV':is_atv_or_btv(output[2][k],int(i[:5]),output[1][k]/100)})
        file_name = Path(filename1)
        file_namee= Path(filename2)
        if file_name.exists()==False:
            with open(filename1, 'w') as csvfile: 
                writer = csv.DictWriter(csvfile, fieldnames = fields) 
                writer.writeheader() 
        with open(filename1, 'a') as f_object:
            dictwriter_object = csv.DictWriter(f_object, fieldnames=fields)
            for k in range(len(mydict)):
                if output[2][ini[k]]>0:
                    dictwriter_object.writerow(mydict[k])
            f_object.close()
        if file_namee.exists()==False:
            with open(filename2, 'w') as csvfile: 
                writer = csv.DictWriter(csvfile, fieldnames = fields) 
                writer.writeheader() 
        with open(filename2, 'a') as f_object:
            dictwriter_object = csv.DictWriter(f_object, fieldnames=fields)
            for k in range(len(mydict)):
                if output[2][ini[k]]<0:
                    dictwriter_object.writerow(mydict[k])
            f_object.close()
        print("ANALYSIS DONE FOR: ",i)
    summarize_anlys(date,month,ecp,done_idx)
    df = pd. read_csv(filename1)
    sorted_df = df.sort_values(by=["Lead/Lag Time"])
    sorted_df. to_csv(filename1, index=False)
#     ff(date,month,filename1,filename2)
    df = pd. read_csv(filename2)
    sorted_df = df.sort_values(by=["Lead/Lag Time"])
    sorted_df. to_csv(filename2, index=False)
#PLEASE DEFINE THE GLOBAL VARIABLES IN THE MAIN SCRIPT BEFORE USING THIS MODULE
# done_idx as an empty list
# $opt_dict and fut_dict as dictionary for storing the filename for various futures and options file
