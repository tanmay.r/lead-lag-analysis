#!/usr/bin/env python
# coding: utf-8

# In[3]:


import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import csv



def date_csv_format(date,month,year=2021):
    return str(date)+'/'+str(month)+'/'+str(year%100)







# fn1 and fn2 are the filenames consisting of upward and downward movement
# fv is the acronym for 500_strikes and nfv for non_500
# fn3 is the file where you want to store the info
# fv_1,nfv_1 and fv_2,nfv_2 are for upward and downward movement respectively

def analz_non_500_vs_500_strikes(date,month,fn1,fn2,fn3):
    fv_1=0
    fv_2=0
    nfv_1=0
    nfv_2=0
    fields=["Date","Up_nfv","Up_fv","Up_Perc_nfv","Up_Perc_fv","Dn_nfv","Dn_fv","Dn_Perc_nfv","Dn_Perc_fv","Tot_nfv","Tot_fv","Tot_Perc_nfv","Tot_Perc_fv"]
    for fn in [fn1,fn2]:
        data=pd.read_csv(fn)
        fp=conv_list(data,"Future Price")
        fm=conv_list(data,"Fut Move")
        dic={}
        oppr=conv_list(data,"Option")
        for i in range(len(fp)):
            if str(fp[i])+str(fm[i]) not in dic:
                dic[str(fp[i])+str(fm[i])]=[0,0]
            if int(oppr[i][:5])%500==0:
                dic[str(fp[i])+str(fm[i])][1]=1
            else:
                dic[str(fp[i])+str(fm[i])][0]=1
        nfv=0
        fv=0
        for i in dic:
            if dic[i][0]==1:
                nfv+=1
            if dic[i][1]==1:
                fv+=1
        if(fn==fn1):
            nfv_1=nfv
            fv_1=fv
        else:
            nfv_2=nfv
            fv_2=fv
    file_name=fn3
    nfv_1_per=(nfv_1)/(nfv_1+fv_1)
    fv_1_per=(fv_1)/(nfv_1+fv_1)
    nfv_2_per=(nfv_2)/(nfv_2+fv_2)
    fv_2_per=(fv_2)/(nfv_2+fv_2)
    tot_fv=fv_1+fv_2
    tot_nfv=nfv_1+nfv_2
    tot_nfv_per=(tot_nfv)/(tot_nfv+tot_fv)
    tot_fv_per=(tot_fv)/(tot_nfv+tot_fv)
    mydict=[{"Date":date_csv_format(date,month),"Up_nfv":nfv_1,"Up_fv":fv_1,"Up_Perc_nfv":nfv_1_per,"Up_Perc_fv":fv_1_per,"Dn_nfv":nfv_2,"Dn_fv":fv_2,"Dn_Perc_nfv":nfv_2_per,"Dn_Perc_fv":fv_2_per,"Tot_nfv":tot_nfv,"Tot_fv":tot_fv,"Tot_Perc_nfv":tot_nfv_per,"Tot_Perc_fv":tot_fv_per}]
    if Path(file_name).exists()==False:
        with open(file_name, 'w') as csvfile: 
            writer = csv.DictWriter(csvfile, fieldnames = fields) 
            writer.writeheader() 
    with open(file_name, 'a') as f_object:
        dictwriter_object = csv.DictWriter(f_object, fieldnames=fields)
        for k in range(len(mydict)):
            dictwriter_object.writerow(mydict[k])
        f_object.close()
        
#for_storing the info in file
# fn1 is the filename where one wants to store the info
def summarize_anlys(date,month,ecp,done_idx,fn1):
    pos=0
    neg=0
    r_pos=0
    r_neg=0
    for i in range(len(done_idx)):
        if ecp[i][0]>0:
            pos+=1
            if done_idx[i]==1:
                r_pos+=1
        else:
            neg+=1
            if done_idx[i]==1:
                r_neg+=1
    
    
    mydict=[{"Date":date_csv_format(date,month),"No of Up inst. analyzed":pos,"No of Up inst. lead/lag holds for":r_pos,"Percentage_up":(r_pos/pos)*100}]
    mydict[0].update({"Date":date_csv_format(date,month),"No of Dn inst. analyzed":neg,"No of Dn inst. lead/lag holds for":r_neg,"Percentage_dn":(r_neg/neg)*100})
    mydict[0].update({"Date":date_csv_format(date,month),"tot no of inst. analyzed":neg+pos,"tot no of inst. hyp. holds for":r_neg+r_pos,"Percentage_tot":(r_neg+r_pos)/(pos+neg)*100})
    fields=["Date","No of Up inst. analyzed","No of Up inst. lead/lag holds for","Percentage_up","No of Dn inst. analyzed","No of Dn inst. lead/lag holds for","Percentage_dn","tot no of inst. analyzed","tot no of inst. hyp. holds for","Percentage_tot"]
    # for key in mydict:
    #     fields.append(key)
        
    file_name=fn1
    if Path(file_name).exists()==False:
        with open(file_name, 'w') as csvfile: 
            writer = csv.DictWriter(csvfile, fieldnames = fields) 
            writer.writeheader() 
    with open(file_name, 'a') as f_object:
        dictwriter_object = csv.DictWriter(f_object, fieldnames=fields)
        for k in range(len(mydict)):
            dictwriter_object.writerow(mydict[k])
        f_object.close()


# In[ ]:




