#!/usr/bin/env python
# coding: utf-8

# In[3]:


import os
import sys
import requests
import time
import requests_toolbelt
from requests_toolbelt.multipart import decoder
import pandas as pd

def fetch(date,opt="",x=0,deadline="",month="APR"):
    fn=[]
    if(x==0):
        response = requests.get("http://172.16.100.192:5002/data/tick/nse/fo?queryString=BANKNIFTY21"+month+"FUT&startDateTime="+date+"%2009:32:00&endDateTime="+date+"%2009:42:00&mtickSize=5")
    else:
        if(opt=="PE"):
            if(deadline!=""):
                response = requests.get("http://172.16.100.192:5002/data/tick/nse/fo?queryString=BANKNIFTY"+deadline+str(x)+"PE&startDateTime="+date+"%2009:32:00&endDateTime="+date+"%2009:42:00&mtickSize=5")
            else:
                response = requests.get("http://172.16.100.192:5002/data/tick/nse/fo?queryString=BANKNIFTY21"+month+str(x)+"PE&startDateTime="+date+"%2009:32:00&endDateTime="+date+"%2009:42:00&mtickSize=5")

        else:
            if(deadline!=""):
                response = requests.get("http://172.16.100.192:5002/data/tick/nse/fo?queryString=BANKNIFTY"+deadline+str(x)+"CE&startDateTime="+date+"%2009:32:00&endDateTime="+date+"%2009:42:00&mtickSize=5")
            else:
                response = requests.get("http://172.16.100.192:5002/data/tick/nse/fo?queryString=BANKNIFTY21"+month+str(x)+"CE&startDateTime="+date+"%2009:32:00&endDateTime="+date+"%2009:42:00&mtickSize=5")


    multipartData = decoder.MultipartDecoder.from_response(response)
#     print('Decode end time ', time.time())
    for part in multipartData.parts:
        headers = dict(part.headers)
        contentDescription = str(headers[b'Content-Disposition'])
        fileNamePart = contentDescription.split(';')[2]
        fileName = fileNamePart.split("\"")[1]
        fileName = fileName.split("/")[-1]
        fn.append(fileName)
        with open(os.path.join(os.getcwd(), fileName), 'wb') as fileHandle:
            fileHandle.write(part.content)
        return fn


# In[ ]:




